# AmazonWeb

This is my submission for an automation challenge

In order to generate a report you will need to use the following command in terminal

***python challenge.py > TestReport.html***

There are 5 tests that are run. 

***CTA, Sign in, Search, Search (No results), Add to cart***

It uses Chrome Webdriver and can be modified to use ones for firefox and internet explorer if needed. 

