import unittest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import HTMLTestRunner

class TestSuite(unittest.TestCase):

    username = "automationtestamazon@gmail.com"
    password = "Password_123"
    validresult = "memory card"
    invalidresult = "iasid132d90ju"

    @classmethod
    def setUp(inst):
        #create a new Firefox session
        inst.driver = webdriver.Chrome('../drivers/chromedriver.exe')
        #inst.driver.maximize_window()
        # navigate to the home page for each test
        inst.driver.get("http://www.amazon.ca/")

    def test_sign_in_cta(self):
         # Find CTA on top right of screen
         self.logoutIfLoggedIn()
         self.actualString = self.driver.find_element_by_id("nav-signin-tooltip")
         self.assertIn("Sign in",self.actualString.text)

    def test_sign_in(self):
        #Find top right sign in button and click it
        self.logoutIfLoggedIn()
        self.driver.find_element_by_id("nav-link-yourAccount").click()
        self.driver.implicitly_wait(1000)
        # Input username and password
        self.login(self.__class__.username,self.__class__.password)
        # Notice how it says username rather than hello sign in on top right
        self.assertIn("Hello, Test", self.driver.find_element_by_id("nav-link-yourAccount").text)

    def test_search(self):
        #Find search box, input string and make sure results are shown
        self.searchbox = self.driver.find_element_by_id("twotabsearchtextbox")
        self.searchbox.send_keys(self.__class__.validresult)
        self.driver.find_element_by_css_selector(".nav-search-submit").click()
        self.showresults = self.driver.find_element_by_id("s-result-count")
        self.assertIn("results for",self.showresults.text)

    def test_no_search_results(self):
        #Find search box and input random string which has no results
        self.searchbox = self.driver.find_element_by_id("twotabsearchtextbox")
        self.searchbox.send_keys(self.__class__.invalidresult)
        self.driver.find_element_by_css_selector(".nav-search-submit").click()
        self.noresults = self.driver.find_element_by_id("noResultsTitle")
        self.assertIn("did not match any products",self.noresults.text)

    def test_add_to_cart(self):
        #Find search box, search for item, add item to cart, login and check to see if you are on shipping page
        self.searchbox = self.driver.find_element_by_id(("twotabsearchtextbox"))
        self.searchbox.send_keys("memory card")
        self.driver.find_element_by_css_selector(".nav-search-submit").click()
        self.card = self.driver.find_element_by_xpath('//*[@title ="SanDisk Extreme Pro 64GB SDXC UHS-I Memory Card (SDSDXXG-064G-GN4IN)" ]').click()
        self.clickIdBtn("add-to-cart-button")
        self.driver.implicitly_wait(500)
        self.clickIdBtn("hlb-ptc-btn-native")
        self.login(self.__class__.username,self.__class__.password)
        self.assertEquals("Enter the shipping address for this order",self.driver.title)

    @classmethod
    def tearDown(inst):
        inst.driver.quit();

    def login(self, username, password):
        username = self.driver.find_element_by_id("ap_email")
        username.send_keys(self.__class__.username)
        password = self.driver.find_element_by_id("ap_password")
        password.send_keys(self.__class__.password)
        self.clickIdBtn("signInSubmit")

    def logoutIfLoggedIn(self):
        self.userLogInfo = self.driver.find_element_by_id("nav-link-yourAccount").text
        # print self.userLogInfo to see if username is present or not
        if self.userLogInfo.find("TestAutomation") == -1:
            print "User not logged in"
        else:
            print "User logged in. Logging out..."
            self.actions = ActionChains(self.driver)
            self.actions.move_to_element(self.driver.find_element_by_id("nav-link-yourAccount")).perform()
            self.driver.find_element_by_id("nav-item-signout").click()
            self.driver.get("http://amazon.ca")

    def clickIdBtn(self,elem):
        self.driver.find_element_by_id(elem).click()

if __name__ == '__main__':
    HTMLTestRunner.main()
